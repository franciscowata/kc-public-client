package com.example;

import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ApiController {

	@Autowired
	KeycloakService keycloakService;
	
	@Value(value = "${host.url}")
	private String hostURL;
	@Value(value = "${app.index.url}")
	private String indexURL;
	

	@GetMapping(path = "/public/index")
	public String showIndex(HttpServletRequest request, Principal principal) {
		// keycloakService.readPermissionsFromToken(request);
		// String response = keycloakService.callBackEndEndpoint(request);
		return "external";
	}

	@GetMapping(path = {"/public/login", "/public"})
	public String loginSeite(Model model, HttpServletRequest request, Principal principal) {
		model.addAttribute("hostURL", hostURL);
		model.addAttribute("indexURL", indexURL);
		return "login";
	}
	
	@GetMapping(path = "/logout")
	public String logout(Model model, HttpServletRequest request) throws ServletException {
		request.logout();
		model.addAttribute("hostURL", hostURL);
		model.addAttribute("indexURL", indexURL);
		return "login";
	}

}