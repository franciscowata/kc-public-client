package com.example;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

@Service
public interface KeycloakService {

	public String callBackEndEndpoint(HttpServletRequest request);

	public void readPermissionsFromToken(HttpServletRequest request);

}
