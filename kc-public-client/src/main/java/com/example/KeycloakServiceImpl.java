package com.example;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.keycloak.AuthorizationContext;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.authorization.Permission;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.stylesheets.MediaList;

public class KeycloakServiceImpl implements KeycloakService {

	private static final String PASSWORD_RESET = "User password has been reset";
	@Value("${keycloak.auth-server-url}")
	private String baseUrl;
	@Value("${keycloak.realm}")
	private String realm;
	@Value("${keycloak.resource}")
	private String client;

	@Value("${realm-admin-user}")
	private String realmAdminUser;
	@Value("${realm-admin-password}")
	private String realmAdminPassword;
	
	private String BEARER_ONLY_ENDPOINT = "http://localhost:8084/bearer/customers";
	private String KEYCLOAK_CERTS_ENDPOINT  = "http://localhost:8080/auth/realms/hello-world-auth/protocol/openid-connect/certs";
	
	private Keycloak createKeycloakInstance(String url, String realm, String user, String password, String client) {
		return Keycloak.getInstance(url, realm, user, password, client);
	}

	@Override
	public String callBackEndEndpoint(HttpServletRequest request) {
		System.out.println("in callBackEndEndpoint() public app");
		KeycloakSecurityContext secContext = getKeycloakSecurityContext(request);
		System.out.println("secContext.getTokenString in public app: "+ secContext.getTokenString());
		
		ResponseEntity<String> response = callCustomersEndPoint(secContext);
		return response.getBody();
	}
	
	@Override
	public void readPermissionsFromToken(HttpServletRequest request) {
		List<Permission> parsePermissions = parsePermissions(request);
		
		
	}


	private List<Permission> parsePermissions(HttpServletRequest request) {
		AuthorizationContext authorizationContext = getAuthorizationContext(request);
		List<Permission> permissions = authorizationContext.getPermissions();
		return permissions;
		
	}

	private ResponseEntity<String> callCustomersEndPoint(KeycloakSecurityContext secContext) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		List<Charset> acceptableCharsets = new ArrayList<>();
		acceptableCharsets.add(Charset.forName("UTF-8"));
		headers.setAcceptCharset(acceptableCharsets);
		String idTokenString = secContext.getIdTokenString();
		headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + idTokenString);
		
		HttpEntity<String> httpRequest = new HttpEntity<String>("{access_token:"+ idTokenString +"}", headers);
//		ResponseEntity<String> response = restTemplate.postforEntity(BEARER_ONLY_ENDPOINT, String.class, httpRequest);
		ResponseEntity<String> response = restTemplate.postForEntity(BEARER_ONLY_ENDPOINT, httpRequest, String.class );
		
		
		return response;
	}


	private ResponseEntity<String> callCertsEndPoint(KeycloakSecurityContext secContext) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("Authorization", "Bearer " + secContext.getIdTokenString());
		HttpEntity<MultiValueMap<String, String>> httpRequest = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		ResponseEntity<String> response = restTemplate.getForEntity(KEYCLOAK_CERTS_ENDPOINT, String.class, httpRequest);
		return response;
	}


	private List<Permission> readPermissions(AuthorizationContext context) {
		return context.getPermissions();
	}


	private AuthorizationContext getAuthorizationContext(HttpServletRequest request) {
		KeycloakSecurityContext secContext = getKeycloakSecurityContext(request);
		return secContext.getAuthorizationContext();
	}


	private KeycloakSecurityContext getKeycloakSecurityContext(HttpServletRequest request) {
		System.out.println("in getKeycloakSecurityContext() public app");
		return (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
	}

	

	
	
	
}

